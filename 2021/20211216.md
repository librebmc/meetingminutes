---
title: Meeting Minutes 2021-12-16
date: 2021-12-16
attendees:
  - toddrosedahl,ibm
  - toshaanbharvani,vantosh
  - michaelgielda,antmicro
  - karolgugala,antmicro
draft: false
---

# LibreBMC SIG Meeting

Meeting date: 16 December 2021
Access link: https://zoom.us/j/91597478078
Meeting ID: 91597478078


# Call to Order

### Anti-trust Reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines.  Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines.  Copies of the Antitrust Guidelines are available at: [Antitrust Guidelines](https://drive.google.com/drive/folders/1k9F3ww5cQL30xQvUL1dt4DE5qLVLax_J?ths=true) in Google Drive or  [Antitrust Guidelines OPF](https://members.openpowerfoundation.org/wg/OPF/document/498) in OpenPOWER Foundation member area.
New version [HERE](https://files.openpower.foundation/s/k5Hny649q3XHSqk)


# AGENDA

 
  * Follow-up Items from last meeting:
      * Note: We all need to use the slack channel to communicate between meetings
      * Encourage all to read the minutes from the last meeting:  Tim Ansell gave his OCP conference report
     
      * Did **Antmicro** receive both the purchased system and the loaner?
          * If so, is there any update on Bringup
          * **Did get both Systems**
          * **One is set up with AST2500 running, not the DC-SCM card**
      * Is there a way around the issue of having a ROT installed in order to load the FPGA from the flash? A "jumper card" for instance?  Follow up requested here from **Antmicro**.
          * **This is purposefully done by design for security**
          * **A small jumper board is in the works, but not ready yet.  Will be opened up as well**
          * **Karol to send an email on how to bypass the ROT**
          * **A bringup board is ready -- DC-SCM card plugs into that for bringup.**
      *  Is there an update on on Lattice DC-SCM v1.0 board with **Antmicro**?
          *  **PCBs have been sent to production, but not built yet.**
          *  **Have the lattice FPGAs, but still need other parts**
          *  **This is not super high priority.  We think ECP5 is better since the toolchain is better (more open).  But Xylinx toolchain is getting better and maybe the report that it is worse may be old information**
     

## Getting hardware to people -- Update

There are 3 components: host system (AC922), Interposer board, DC-SCM Board.

 * AC922
     * 1 is in enroute to Austin
     * 1 is in Australia
     * 1 is in Sunnyvale, USA @ Google
     * 1 is in Seattle, USA @ Google
     * Loaner from IBM Poland to Antmicro is in process
         * **Delivered**
     * Further action on aquiring AC922s
         * Antmicro is purchasing one and should have it soon
             * Snags have been worked through and reports are it should arrive soon
             * **Delivered**
         * VanTosh will also be purchasing at least one
             * received and in testing


 * Interposer : https://git.openpower.foundation/librebmc/ac922interposer)
     * 6 pieces made.
         * 1 is in Rochester
         * 1 with Todd
             * Should I ship one to VanTosh now?
                 * This makes sense.  Ship the DC-SCM and interposer
                     * **Not shipped.  Will do next year.**
                     * **Toshaan is bringing up microwatt/OpenBMC on a stand alone Arty A7**
             
         * 2 are in Australia
         * 2 are at Antmicro
     * Parts for 4 more ordered.  Six different components.
         * 5 of the 6 have been delivered
         * 1 part is on backorder from Mouser.  Outlook 10/22.  Todd trying to find alternate parts.  **No further progress**

 *  DC-SCM board
     *  4 boards, but only 3 have FPGAs
         *  Google is providing some to Antmicro.
             *  Still waiting for parts for 4th
         *  Antmicro shipped one to Australia
             *  Arrived and is in bring-up
            


     *  Need to start working on getting Lattice parts for the DC-SCM board.  Michael will take the lead on this.
         *  PCBs sent in for production for lattice FPGA
         *  Still need memory and ethernet parts
         *  Marshall offered to help if there are issues getting FPGAs

## OCP DC-SCM 2.0 Working Group -- Meets bi-weekly

* Todd will keep tabs and keep this group posted.  However, only after public meetings have been held on the topics.
* **Antmicro** -- Is there interest in joining the OCP DC-SCM 2.0 WG effort?
    * **Todd to send an email to the OCP WG leader asking how Antmicro can join**
* Todd to run a call with Google and some other key players to discuss our common direction

# Conference Reports
* **Todd will show a one pager here to summarize**
* Outcome was that Todd is to schedule LibreBMC calls next year with the following:
  * Kameleon
      * Doing an FPGA based ASIC/ROT for security
  * AMI
  * Ampere
  * ASPEED
  * RunBMC FPGA
  * GOWIN

## Gateware

* **Any updates here?** 

## Software

 * **Any updates here?**


## Workgroup Collaboration Tools

 * [Meeting Minutes](https://meetingminutes.openpower.foundation/librebmc/)
   https://meetingminutes.openpower.foundation/librebmc/
   
 
 * Meeting invites are still not seeming to get mailed?
     * Long term TODO : Need to make the system actually sends emails with ical attachments.  It will take awhile to make this work.  Rather we should have interested parties import the cal and get notifications.
    
     * [WebSys Document](https://files.openpower.foundation/s/Sj756P5B39T7XnP)
     * Meeting invites should include a link to this agenda so everyone can add agenda topics.  Todd to add to the Australia time invite.  Michael/Karol to add to the Europe time invite
     * **Toshann is testing the automatic meeting notice generation**

- OPF Discuss (with calendar and link to chat)
https://discuss.openpower.foundation/c/sig/librebmc/11
webcal://discuss.openpower.foundation/c/sig/librebmc/l/calendar.ics

- Slack / IRC / Mattermost
https://openpowerfoundation.slack.com/archives/C01UVKFKUQY
#librebmc on libera (namespace)
https://chat.openpower.foundation/opf/channels/librebmc 

- OPF Files
https://files.openpower.foundation/s/iZRseq3XLtRcjtX

- OPF Discuss
https://discuss.openpower.foundation/c/sig/librebmc/11 

- OPF GIT repository
https://git.openpower.foundation/librebmc/librebmc 
Will be mirrored to github and gitlab

- OPF Kanban (linked with chat)
https://kanban.openpower.foundation/b/hgDqwnbiZDHFR3B3b/librebmc


 
## New Topics?

* How do we get more people involved? **Nothing new here**
    * Need to get ready images to consume by people
    * Need LiteX-Hub support and integration
    * Add more FPGA boards supports
    * Get it running on Raspbery PI
    * Then add some documentation
    * Badges and certificates

* It was suggested that we should have a logo for libreBMC.  Any thoughts from the team.  Nice to have, but should not be a focus right now.  **Any ideas on this yet?**
* Open the software stack to the world **This is not new, but worth reminding us again of the next steps**
    * Step 1:  Get the FPGA/DC-SCM card booting the AC922
    * Step 2:  Incorporate microwatt into lite-x natively.
        
    * Step 3:  Enable others
        * Need others to be able to re-create the results.
        * Build all pieces -- Core, peripherals, OpenBMC,etc
        * Need official OpenBMC project and a makefile, bitbake/etc
    * Step 4:  Include the firmware into the CI infrastructure
    * Step 5:  Enhancements -- hoping for further industry participation
        * Full FPGA support (I2C, FSI, etc) More than just bit banging
        * Resource usage -- Shrink micro-watt to get 4 cores.
            * See requirements from Tim A. on last call
        * Full OpenBMC support expected for a standard server


## Next Meeting

{{< localdatetime date="2022-01-13" time="16:00" >}}
