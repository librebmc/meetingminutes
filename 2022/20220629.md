---
title: Meeting Minutes 2022-06-29
date: 2022-06-29
attendees:
  - toddrosedahl,ibm
  - toshaanbharvani,vantosh
  - antonblanchard,ibm
  - michaelneuling,ibm
  - munirahmad,lattice
  - lukeleighton,libre-soc
  - konstantinous,vectorcampgr
  - andrey,libre-soc
  - cesar,libre-soc
  - jacob,libre-soc
  - timpearson,raptorengineering
  - paulmackerras,ibm
draft: false
---

# LibreBMC SIG Meeting

Meeting date: 29 June 2022
Access link: https://zoom.us/j/91597478078
Meeting ID: 91597478078


# Call to Order

### Anti-trust Reminder

This is a reminder that all OpenPOWER Foundation activities are subject to strict compliance with the OpenPOWER Foundation’s Antitrust Guidelines.  Each individual participant and attendee at this meeting is responsible for knowing the contents of the Antitrust Guidelines, and for complying with the Antitrust Guidelines.  Copies of the Antitrust Guidelines are available at: [Antitrust Guidelines](https://files.openpower.foundation/s/k5Hny649q3XHSqk)

### Meeting Recording

Meeting is being recorded

# AGENDA

## Introduction

New folks this week?

## New News

Any update?

## Hardware Update





## Bringup

* AntMicro has all the HW it needs to do bring-up on the AC922.
    * However, they have very little time to spend on this
* AntMicro will send a DC-SCM card to Toshaan
* Todd will send an Interposer to Toshaan
* **I have not yet done this.  Does it still make sense?**
    * Yes, Toshaan still has time and resources available



## OCP DC-SCM 2.0 Working Group -- Meets bi-weekly

 **Todd** to run a call with Google, Munir from Lattice, and some other key players to discuss our common direction for the 3.0 standard. WIP.
 * DC-SCM 2.0 Has been submitted

## Conferences


* **Todd** Still working to set up various follow-ups
* **We need something new to showcase at OCP22 and SC22**

**Overall Goal Reminder:**  We want to boot an AC922 using an FPGA (not the AST2500 ASIC) on a DC-SCM card.  We want to prove that the FPGA can boot a high power, modern server.  

To do this, we need the following pieces:
1.	A DC-SCM card that has an FPGA on it to replace the AST2500 ASIC
a.	Antmicro built a few of these that use Xylinx A7s.  They power on, but no bring-up has been done to show they will boot the system.
b.	Antmicro is building some that have ECP5’s on them, but they are still missing some parts
2.	An interposer that will plug into the DC-SCM card and the AC922.
a.	I have 6 of these built
3.	A Root of Trust bypass jumper
a.	Plans for these are available and they are easy to build
4.	A softcore (microwatt) running on the FPGA
a.	This works, but it needs to be better incorporated with Lite-x
b.  Can use libreSOC.
    **Yes, we think so as Libre-SOC is a drop-in replacement (more or less)**
c.  Can use Kestral
    * Raptor has PCI-e connected. ECP5 based cards booting up a blackbird.
    * Runs Zepher RTOS on microwatt. Does not run linux.
        https://www.zephyrproject.org/
    * Next step to add graphical output (framebuffer)
        * rgb-ttl driver : https://github.com/RoaLogic/vga_lcd
        lukeleighton has (2012) linux kernel driver source for this
    * DC-SCM to SO-DIMM interposer card (possibility)
    * Raptor could make a DC-SCM card and interposer to boot a Kestrel from the DC-SCM card
5.	Full gateware in Lite-x for the target FPGA is needed such that the entire OpenBMC stack can run
a.	Some modules are done.  Some not.  I have the list.
6.	Full OpenBMC code running
a.	We currently power on our prototype with scripts.  OpenBMC is not running.
        * OpenBMC runs YOCTO
        https://www.yoctoproject.org/

So far we did a prototype where we used the xilinx A7 FPGA on a custom breadboard and ran microwatt and a very stripped down FPGA.  We proved it could boot the AC922, but again it was very low function.  We need to have it fully functional to really prove it.

* Getting to the overall goal above by **OCP22 (Oct 18th)** seems unachievable at the current pace

* **Other options:**
    * Show the bringup of the real DC-SCM card (A7) on the AC922.  Show it can boot, but use scripts as we did prior.  OpenBMC not running.  Very few gateware additions.
    * Pivot to the ECP5 using OpenBMC on the DC-SCM card.
        * Gateware is all there (per Raptor)
            * lite-x, n-migen, nextpnr-ecp5, yosys. all Libre-Licensed
        * Open-Source tooling is better
            * Usable with no legal issues
    * Do a simulation only -- better to use real hardware
        * Could have 2 FPGAs
        * 1 is the BMC
        * The other FPGA could be some sort of a representation of the system
            * Implement HB on it?
            * Run QEMU?
            * Some other simulation running?

## Communication / Collaboration


 * Calendar invites are working now it seems.
     * Cal invites did not work.  Todd/Toshaan to work offline on it.
     * **Cal invites did not work again -- at least on outlook** 

 * Standing Reminder:  Everyone should be posting things into the #librebmc-sig slack channel.
 * **I created new repos on git in order to be able to point people at the information they need to get started and help out**
 * https://git.openpower.foundation/librebmc
  

## Gateware

Update?

 * Any progress on getting https://github.com/litex-hub/linux-on-litex-power used?
 * Any progress on making sure that MicroWatt works inside LiteX?
 * **No progress has been made here of late**


## Simulation

 * Renode looks like a good option for a simulation environment for LibreBMC.
     * **Todd** to send note to Piotr with information on what we would like to do with Renode and where the links are to the code/etc.  **WIP**
   
## Soft Cores

 * OPF is funding an FPGA optimized POWER soft core 
     * Target was "VexRISCV" resource usage and performance. **Todd to schedule a readout at a future meeting.  **Toshaan to check on when they could present**

## Toolchain

Updates?

## Software

Updates?

## FPGA Usage Barriers

* List of opens (potential barriers) for using FPGAs as BMCs
    * Cost -- 
        * Projection is that will be cost competitive
        * Some things require an external chip
            * video driver, but could be added later
    * Soft Error Rates -- **Munir** to follow up
        * Hard fails roughly the same as an ASIC
        * Looks like Xilinx SER FIT is reasonable (<200).
        * And detectable and fixable with an image reload
        * Lattice to provide data on FIT rates and recovery design
            * Munir did send an email with info
    * **Performance?  8X slower than an ASPEED?**
        * **Information from Lattice**
            * much faster to BMC to boot.  2min for ASPEED. 5sec for FGPA
                * This is mostly a function of the BMC stack, the ASPEED vs FPGA, so potentially not Apples to Apples (ASPEED vs FPGA)
            * **Lattice to provide some information on this performance comparisons to ASPEED**
            * **How long will it take the system to boot on the FPGA?**
            * **Opening up of LTPI is under consideration (MIT)**
    * Image size 
            * 85,000 latches ECP5
            * New chips coming.  See last week's minutes
            * How to optimize image space?
                * Currently microwatt fits easily in 85,000 latches
                * https://github.com/antonblanchard/microwatt
                * our CI results are here
                * https://github.com/antonblanchard/microwatt/actions/runs/2205354319
                * https://github.com/antonblanchard/microwatt/blob/master/.github/workflows/test.yml#L70

## Project Ideas -- Running list of areas where we could use help

* **Near term project ideas -- things that could be worked on now**
    * Todd/Toshaan to get a page going on the OPF web page that we can point people to.
        * **This effort has been started**
        * **We will use GIT for all issues**
        * **We will have a "projects" page.  We need the issues within the projects to be broken down into small pieces.**
        * **Toshaan to send Todd me an email with instructions/example on how to start**

    * Linux-on-Litex-VexRiscV => Linux-on-Litex-Microwatt (/cc Anton)
        * **Todd to talk to Joel**
        * https://github.com/litex-hub/linux-on-litex-power
        * There is a demo showing linux running on VexRiscV
            * https://github.com/litex-hub/linux-on-litex-vexriscv
        * Replicate that demo using Microwatt
        * https://antmicro.com/blog/2020/05/multicore-vex-in-litex/
        * This can be started now and questions asked/answered in the open
            * Ask questions on github or on IRC #litex channel on Libera

    * Add emulation of LPC peripheral to Renode (/cc Piotr)
        * Questions asked/answered github
        * Karol/Piotr can help as well directly
    
    * Get a working litex configuration for microwatt
        * microwatt is an option in litex, but it is not complete.  Need the interrupt controller working and linux booting
            * Need to just try booting linux, see what breaks,and fix it
            * Joel Stanley would be a contact
    
     * Add Full OpenBMC support for the AC922
        * Fork the Witherspoon OpenBMC and run it -- see what breaks

    * Add Full Litex FPGA support (I2C, FSI, etc) More than just bit banging:
        * Tim did this in the past --> https://docs.google.com/document/d/10RJUk_uXhku6FTnb-WtpIDHrkrEaWdAREF4wafYzfV4/edit
    a.	PCIe - https://github.com/enjoy-digital/litepcie
    b.	I2C/I3C - 
    c.	USB - https://luna.readthedocs.io/
    d.	FSI (uses I3C)
    e.	GPIOs - https://github.com/enjoy-digital/litex/blob/master/litex/soc/cores/gpio.py
    f.	LPC -- We have the gateware, but need integration with Litex
    g.	Ethernet - https://github.com/enjoy-digital/liteeth
    h.	Refclock
    i.	CLKIN
    j.	JTAG - https://github.com/enjoy-digital/litex/blob/master/litex/soc/cores/jtag.py
    k.	DDR - https://github.com/enjoy-digital/litedram/tree/master/litedram
    l.  SPI - https://github.com/litex-hub/litespi
    * Documentation (/cc all)
    

    * Add CI so we know when litex breaks
        * See FPGA CI from Antmicro
            * https://github.com/chipsalliance/f4pga-examples
            * https://builds.antmicro.com/results/invocations/f2f9d40b-e7d7-4dde-a3c3-4a9271a5bfaf
            * https://builds.antmicro.com/results/invocations/f2f9d40b-e7d7-4dde-a3c3-4a9271a5bfaf?target=xc7_counter_test_arty_35
            * https://builds.antmicro.com/results/invocations/f2f9d40b-e7d7-4dde-a3c3-4a9271a5bfaf?target=xc7_litex_demo_vexriscv_arty_35
        * See work from Carl Karsten <cfkarsten@gmail.com> - https://docs.google.com/document/d/1jyodwCNK-9mokDAe6X1AMhEDPW-qPeqPTHI-z9aUGVg/edit#
            * **Could point students to Carl**
        * See recent work from Enjoy Digital - https://twitter.com/enjoy_digital/status/1514192833070174208?s=20
    * We have an ecosystem of :micropython, u-boot, linux.
        * All work, but only in specific configs.  Documenting and testing those configurations would be a good project.
        * Could use Zephyr
        * https://antmicro.com/blog/2021/11/renode-zephyr-dashboard-100-boards/
        * https://antmicro.com/blog/2022/03/test-driven-development-of-zephyr-micro-ros-with-renode/
        * https://antmicro.com/blog/2019/12/tflite-in-zephyr-on-litex-vexriscv/
    * Define a boot loader for microwatt
        * Make it work across configurations (arty, orangecrab)
   



* **Mid-term ideas -- things that need more definition**

    * Include the firmware into the CI infrastructure
    * Need LiteX-Hub support and integration
    * Add more FPGA boards supports
    * Get it running on Raspbery PI
     * Linux on LiteX VexRISCV like repository
         * An example for just running a POWER Linux system on as many FPGA development boards as possible?
         * https://github.com/litex-hub/linux-on-litex-vexriscv -- >30 boards supported...
         * Repository exists - https://github.com/litex-hub/linux-on-litex-power

     * Improving Ethernet performance.
         * https://github.com/rprinz08/hBPF 


* **Further Project Ideas from Tim:**
     * It would be great to get help with getting GHDL into conda-eda
         * https://github.com/hdl/conda-eda/issues/171
         * https://github.com/hdl/conda-eda/pull/180
     * It would be great to get help with adding POWER support into conda-eda
         * https://github.com/hdl/conda-eda/pull/3
     * It would be great to investigate "EDA Containers" for usage with LiteX
         * https://github.com/hdl/containers/issues/40
         * https://twitter.com/carlosedp

* **NLnet funding / Grant application idea**
     * https://nlnet.nl/entrust/
     * Libre-BMC definitely qualifies for privacy and enhanced trust
     * suggestion: proposal covers *dual* FPGAs connected back-to-back
     * one BMC client, one BMC server
     * one has Libre-BMC, but it uploads linux OS to the *other* FPGA
     * bonus points: swap in an ASPEED 2500 or Nuvoton BMC for interoperability testing
     * primary focus: keep cost down so that more people can use it / collaborate!
     * if proposal is ONLY for EUR 10,000 servers then NLnet will likely go "hmmm" and reject on the basis of "reduced benefit to users"
     * EU aspect strongly recommended but once satisfied, other people world-wide can be "pulled in" and receive funds (e.g. ADG India Group)
     * also recommend inviting 3mdeb (OPF Member, coreboot developers) to collaborate (helps satisfy EU aspect)
     * **Toshaan and Luke will work on this**
     
## Workgroup Collaboration Tools

 * [Meeting Minutes](https://meetingminutes.openpower.foundation/librebmc/)
   https://meetingminutes.openpower.foundation/librebmc/
   
 
 * Meeting invites are still not seeming to get mailed?
     * Long term TODO : Need to make the system actually sends emails with ical attachments.  It will take awhile to make this work.  Rather we should have interested parties import the cal and get notifications.
    
     * [WebSys Document](https://files.openpower.foundation/s/Sj756P5B39T7XnP)


- OPF Discuss (with calendar and link to chat)
https://discuss.openpower.foundation/c/sig/librebmc/11
webcal://discuss.openpower.foundation/c/sig/librebmc/l/calendar.ics

- Slack / IRC / Mattermost
https://openpowerfoundation.slack.com/archives/C01UVKFKUQY
https://chat.openpower.foundation/opf/channels/librebmc
#librebmc on libera (namespace)
https://chat.openpower.foundation/opf/channels/librebmc 

- OPF Files
https://files.openpower.foundation/s/iZRseq3XLtRcjtX

- OPF Discuss
https://discuss.openpower.foundation/c/sig/librebmc/11 

- OPF GIT repository
https://git.openpower.foundation/librebmc/librebmc 
Will be mirrored to github and gitlab

- OPF Kanban (linked with chat)
https://kanban.openpower.foundation/b/hgDqwnbiZDHFR3B3b/librebmc

 
## Community Involvement

* Options for not needing  an AC922
    * FPGA on both sides (emulating the HPM)
        * Raptor has such a board that has an FPGA on both sides
    * Can just use QEMU and simulate the AC922 side
    
* How do we generate more activity/interest
    * Engage Universities -- **There are interested universities**
    * Todd to start a list of universities and contacts **New direction here.  Todd/Toshaan to make an OPF page "Education Page" that points to these projects as well as the OpenPOWER curriculum being developed.  Then we can all point our education contacts to that page. No need to list them here**
    * Need clear work breakdown.  **We have this for some projects.** 
    * Need Mentors. **True for Interns/MLH/etc, but many projects can be supported in the open**
 
    * Offer badges/certificates
    * Offer Bounties
    * Major League Hacking Interships
        * Start end of May.
        * Need to sign up by end of March
        * Must have sponsors to guide students and hold office hours
        * **We missed this window, but the next one starts in Sept and we will circle back on this in a month or so.  We still need to think about detailed work tasks and who can be mentors**
        * To use MLH, We need BoD (Board of Directors) approval
* **Need work items clearly identified and easily understood**
        
    * Documentation -- We need build instructions, readmes, etc
        * Need someone replicate the FPGA/OpenBMC load from scratch
            * Then document the process for others to follow so they can replicate the results
                * Build all pieces -- Core, peripherals, OpenBMC,etc
             * **Toshaan to take a crack at this now**

        
    * Need official OpenBMC project and a makefile, bitbake/etc

    * Need the project broken down into manageable pieces  
  

* It was suggested that we should have a logo for libreBMC.  Any thoughts from the team.  Nice to have, but should not be a focus right now.
    * **I did see an offer of help for a logo.  I will follow up later.  TBD**


 ## Goals -- Need timelines on these -- WIP
* Tasks defined and project broken down
 * Able to generate a bitstream for an FPGA using fully open source toolchain.
 * Have RTL suitable for real production usage that has software support in the upstream OpenBMC project.
 * Someone seriously starting to do a real (non-development) LibreBMC deployment.
 * Fully functional Gateware and OpenBMC code stack for AC922
 * Determine the performance/size

## Next Meeting

{{< localdatetime date="2022-07-14" time="15:00" >}}
